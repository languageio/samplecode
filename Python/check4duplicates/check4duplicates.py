import os
import sys
import re


def find_id_overlap(prev_dir, cur_dir):
    run_dir = os.getcwd()
    prev_dir = run_dir  +  prev_dir
    cur_dir = run_dir  +  cur_dir
    dict1 = {}
    lang_dirs1 = os.listdir(prev_dir)
    # Find all of the ids in the first directory
    for lang_dir in lang_dirs1:
        thing = lang_dir.split("_")
        pos = len(thing) - 1
        lang = thing[pos]
        root_path = os.path.join(prev_dir, lang_dir)
        files = os.listdir(root_path)
        ids = []
        for file in files:
            ids.append(file.split("_")[0])
        dict1.update({lang: ids})

    dict2 = {}
    lang_dirs2 = os.listdir(cur_dir)
    # Find all of the ids in the 1st directory
    for lang_dir in lang_dirs2:
        thing = lang_dir.split("_")
        lang = thing[len(thing) - 1]
        root_path = os.path.join(cur_dir, lang_dir)
        files = os.listdir(root_path)
        ids = []
        for file in files:
            ids.append(file.split("_")[0])
        dict2.update({lang: ids})
    z = []
    for lang in dict1.keys():
        cur_ids = dict2.get(lang)
        for a in cur_ids:
            b = dict1.get(lang)
            if a in b and a != "manifest.xml":
                z.append(a)

    z = sorted(z)
    nonDuplicates = []
    for c in z:
        if c not in nonDuplicates:
            nonDuplicates.append(c)

    for dup in nonDuplicates:
        print(dup)

if __name__ == '__main__':
    fst = "/test_files/FIRST"
    snd = "/test_files/SECOND"
    find_id_overlap(fst, snd)

