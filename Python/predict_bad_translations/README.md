The author of this script would like to predict which words are likely to be mistranslated by machine translation engines using the metadata contained in `raw_data.csv`. They have written a script to train some simple models on their data, but the prediction accuracy for new data isn't nearly as good as expected based on the R^2 scores they generated.

Please do the following:

* Demonstrate how you would explore the data
* Improve upon the data cleaning, feature engineering, model training, and model selection provided in the code
* Suggest next steps (ex. additional data collection, refactoring, other ML models) you would experiment with to improve the code and results
* Explain possible reasons for the bad predictions from the original code

Note:

* The content in raw_data.csv is the only copy of the original data, so that file cannot be modified
* To reduce the scope of the assignment, the author is primarily interested in results from the three types of model already included in the code: linear regression, lasso, and decision tree regressor
