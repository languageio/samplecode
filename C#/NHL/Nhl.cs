﻿/*
 *We have a billionaire client whose passion is to invest in NHL teams that have never won the Stanley Cup.
 *However, she has a phobia of cities whose name starts with a vowel. 
 *She also has a deep appreciation of history and does not consider any team founded later than 1989 worthy of investment. 
 *She has asked use to write a program that will parse two CSV files that only she maintains to generate an output of teams that have never
 *won a Stanley Cup, their city name does not start with a vowel, and they were founded prior to 1990. 
 *She would like this output to be alphabetical (a-z).
 */
using System;
using System.IO;
using System.Collections.Generic;

namespace sample_code
{
    public class Nhl
    {

        public static void Main(String[] args)
        {
            //Process csv file that contains teams and their most recent championship year
            String a = args[0].ToString();
            String b = "";
            String c = ",";
            List<string> z = new List<string>();

            try
            {
                StreamReader d = new StreamReader(a);
                //Loop through csv file
                while ((b = d.ReadLine()) != null)
                {
                    String[] e = b.Split(c);
                    String f = e[0];

                    //Verify there is no championship date supplied
                    if (e.Length < 2 || e[1] == null || "".Equals(e[1]))
                    {
                        //Exclude teams that have city names that start with a vowel.
                        if (!f.Substring(0, 1).Equals("A"))
                        {
                            if (!f.Substring(0, 1).Equals("E"))
                            {
                                if (!f.Substring(0, 1).Equals("I"))
                                {
                                    if (!f.Substring(0, 1).Equals("O"))
                                    {
                                        if (!f.Substring(0, 1).Equals("U"))
                                        {
                                            z.Add(f);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (IOException e)
            {
            }

            //Process csv file that contains teams and their founding year
            String a1 = args[1].ToString();
            String b1 = "";
            String c1 = ",";
            List<string> z1 = new List<string>();

            try
            {
                StreamReader d1 = new StreamReader(a1);
                //Loop through csv file
                while ((b1 = d1.ReadLine()) != null)
                {
                    String[] e1 = b1.Split(c1);
                    String f1 = e1[0];

                    //Verify the team was found prior to 1990 and is in our least of teams that never won a title.
                    if (Int32.Parse(e1[1]) < 1990 && z.Contains(f1))
                    {
                        z1.Add(f1);
                    }
                }
            }
            catch (IOException e)
            {
            }


            //Sort in descending order (z-a)
            String t;
            for (int x = 0; x < z1.Count; x++)
            {
                for (int y = 0; y < z1.Count - x - 1; y++)
                {
                    if (z1[y].CompareTo(z1[y + 1]) > 0)
                    {
                        t = z1[y];
                        z1[y] = z1[y + 1];
                        z1[y + 1] = t;
                    }
                }
            }
            //Output list in alphabetical order
            for (int i = 0; i < z1.Count; ++i)
            {
                Console.WriteLine(z1[i]);
            }
            Console.ReadLine();
        }
    }
}