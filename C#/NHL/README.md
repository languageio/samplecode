# README #

Please refactor the Nhl.cs class located in the following Git repo with a focus on making it more readable and maintainable.

**NOTE: we can not change the input CSV files in any way as they are controlled by the client not us. 
It is also crucial that it produces the same output as it did prior to the refactor.