# README #

Using the MySQL tables defined in mysql_questions.txt please write SQL that returns all vendors with a name containing the full word 'Professional', a translation_type of 'human', and which are associated with the consumer that has the name 'Lenny'.

Using the following MySQL tables please write SQL to remove all vendors with a translation_type of 'machine' associated with the consumer that has an id of '1'.