# README

Please convert the Aura component to a Lightning Web Component.

The LWC should implement the same UI behavior as the Aura component. Adding unit tests to the LWC would be a big plus.
