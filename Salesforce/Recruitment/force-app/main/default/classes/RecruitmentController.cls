public with sharing class RecruitmentController {
    @AuraEnabled(cacheable=true)
    public static List<RecruitmentData> getData(Integer records) {
        List<RecruitmentData> listOfData = new List<RecruitmentData>();
        for (Integer i = 0; i < records; i++) {
            RecruitmentData data = new RecruitmentData();
            listOfData.add(data);
        }

        return listOfData;
    }
}