public class RecruitmentData {
    @AuraEnabled
    public String name;
    @AuraEnabled
    public String website;
    @AuraEnabled
    public Double amount;
    @AuraEnabled
    public String phone;
    @AuraEnabled
    public Datetime closeAt;

    public RecruitmentData() {
        this.name = 'Salesforce';
        this.website = 'www.salesforce.com';
        this.amount = Math.floor(Math.random() * 100);
        this.phone = String.valueOf(Math.roundToLong(Math.floor(Math.random() * 9000000000.00) + 1000000000.00));
        this.closeAt = Datetime.newInstance((Integer) (Datetime.now().millisecond() + 86400000 * Math.ceil(Math.random() * 20)));
    }
}