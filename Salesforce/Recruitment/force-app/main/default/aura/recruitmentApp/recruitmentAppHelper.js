({
    fetchData : function(component) {
        var action = component.get("c.getData");
        action.setParams({
            records: 30
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set('v.data', response.getReturnValue());
            }
            // error handling when state is "ERROR"
            else if (state === "ERROR") {
            }
            // error handling when state is "INCOMPLETE"
        });

        $A.enqueueAction(action);
    }
})
