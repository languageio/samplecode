({
    init : function(component, event, helper) {
        component.set('v.columns', [
            { label: 'Label', fieldName: 'name' },
            { label: 'Website', fieldName: 'website', type: 'url' },
            { label: 'Phone', fieldName: 'phone', type: 'phone' },
            { label: 'Balance', fieldName: 'amount', type: 'currency' },
            { label: 'CloseAt', fieldName: 'closeAt', type: 'date' },
        ]);

        helper.fetchData(component);
    }
})
