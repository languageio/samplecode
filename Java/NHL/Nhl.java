/*
 *We have a billionaire client whose passion is to invest in NHL teams that have never won the Stanley Cup.
 *However, she has a phobia of cities whose name starts with a vowel. 
 *She also has a deep appreciation of history and does not consider any team founded later than 1989 worthy of investment. 
 *She has asked use to write a program that will parse two CSV files that only she maintains to generate an output of teams that have never
 *won a Stanley Cup, their city name does not start with a vowel, and they were founded prior to 1990. 
 *She would like this output to be alphabetical (a-z).
 */
package nhl;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;


public class Nhl {

    public static void main(String[] args) {
        //Process csv file that contains teams and their most recent championship year
        String a = args[0].toString();
        String b = "";
        String c = ",";
        ArrayList<String> z = new ArrayList<String>();
        
        try (BufferedReader d = new BufferedReader(new FileReader(a))) {
            //Loop through csv file
            while ((b = d.readLine()) != null) {
                String[] e = b.split(c);
                String f = e[0];
                
                //Verify there is no championship date supplied
                if (e.length < 2 || e[1] == null || "".equals(e[1])) {
                    //Exclude teams that have city names that start with a vowel.
                    if (!f.substring(0,1).equals("A")) {
                        if (!f.substring(0,1).equals("E")) {
                            if (!f.substring(0,1).equals("I")) {
                                if (!f.substring(0,1).equals("O")) {
                                    if (!f.substring(0,1).equals("U")) {
                                        z.add(f);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (IOException e) {
        }
        
        //Process csv file that contains teams and their founding year
        String a1 = args[1].toString();
        String b1 = "";
        String c1 = ",";
        ArrayList<String> z1 = new ArrayList<String>();
        
        try (BufferedReader d1 = new BufferedReader(new FileReader(a1))) {
            //Loop through csv file
            while ((b1 = d1.readLine()) != null) {
                String[] e1 = b1.split(c1);
                String f1 = e1[0];
               
                //Verify the team was found prior to 1990 and is in our least of teams that never won a title.
                if (Integer.parseInt(e1[1]) < 1990 && z.contains(f1)) {
                    z1.add(f1);
                }
            }
        } catch (IOException e) {
        }
        
        
        //Sort in descending order (z-a)
        String t;
        for (int x = 0; x < z1.size(); x++) {
            for (int y=0; y < z1.size() - x - 1; y++) {
                if (z1.get(y).compareTo(z1.get(y+1)) > 0) {
                    t = z1.get(y);
                    z1.set(y, z1.get(y + 1));
                    z1.set(y + 1, t);
                }
            }
        }
        //Output list in alphabetical order
        for (int i = 0; i < z1.size(); ++i) {
            System.out.println(z1.get(i));
        }
    }
    
}
